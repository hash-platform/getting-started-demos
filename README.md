# Getting started demos

This repository hosts some demos for using hashkern to manage your mono repos, there are demos for Digital Ocean, Google Cloud Platform, Azure and AWS clouds.

To use these demos you need to install hashkern to your machine, please follow installations instructions [here](https://gitlab.com/hash-platform/hashkern/-/tree/main#installing-hash-kern)

## GCP

In order to use the GCP demo you need to create a GCP account [here](https://console.cloud.google.com), create a project and
authemticate locally to your account with gcloud using these command

```bash
gcloud auth login
gcloud auth application-default login
```

Create a project to host your DNS zone, lets say you have a domain called `example.com`, create a new DNS zone in your selected
project with this domain `gcp.example.com`, now point your `gcp.example.com` to the new zone by using the NS servers created by
google in your own registrar.

You will need two GCP projects for dev and staging envs, replace their names in `resource.dev.yaml` and `resource.stage.yaml` in `demos/GCP` directory.

You're ready to deploy your microservice now, using this command, first make sure to follow using the client instructions [here](https://gitlab.com/hash-platform/hashkern/-/tree/main#installing-hash-kern), then execute this command if you installed it from source

```bash
python <path to hash kern main.py> --env=dev deploy demos/GCP/services/test/
```

Or use this command if it was installed from PyPi

```bash
haks --env=dev deploy demos/GCP/services/test/
```

## Azure

In order to use the Azure demo you need to create a Azure account [here](https://portal.azure.com/), create two subscriptions one for dev and
another for staging and authemticate locally to your account with az CLI using these command

```bash
az login
```

Create a new resource group for your DNS zone, lets say you have a domain called `example.com`, create a new DNS zone in your selected
resource group with this domain `az.example.com`, now point your `az.example.com` to the new zone by using the NS servers created by
azure in your own registrar.

You will need two Azure subscriptions for dev and staging envs, replace their ids in `resource.dev.yaml` and `resource.staging.yaml` in `demos/AZ` directory.

You're ready to deploy your microservice now, using this command, first make sure to follow using the client instructions [here](https://gitlab.com/hash-platform/hashkern/-/tree/main#installing-hash-kern), then execute this command  if you installed it from source

```bash
python <path to hash kern main.py> --env=az-dev deploy demos/AZ/services/test
```

Or use this command if it was installed from PyPi

```bash
haks --env=az-dev deploy demos/AZ/services/test
```

## Digital Ocean

In order to use the Digital Ocean demo you need to create a Digital Ocean account by clicking on the link bellow, then you need to create a personal access token from [here](https://cloud.digitalocean.com/account/api/tokens), save the token to an env variable using this command

```bash
export DIGITALOCEAN_TOKEN=<your token here>
```

[![DigitalOcean Referral Badge](https://web-platforms.sfo2.cdn.digitaloceanspaces.com/WWW/Badge%201.svg)](https://www.digitalocean.com/?refcode=73ddf956e716&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge)

And now you are ready to deploy the demo to your Digital Ocean account.

**Hints**: The setup here doesn't provide you with a domain, you need to setup your own domain, the code uses `*.do.mouhsen.de`, please change it to your own domain.

Use this command to deploy the micro service:

```bash
haks --env=do-dev deploy demos/DigitalOcean/services/micro/
```

Once it is done you need to do one thing manually:

* Connect to the cluster by executing the connect command from cloud console, you need to get the IP address of nginx service using this command and assign
it to your domain `app.dev.<your domain>`
