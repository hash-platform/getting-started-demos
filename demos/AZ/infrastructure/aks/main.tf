locals {
  prefix = "${var.prefix}-${var.environment}"
}
module "aks" {
  source  = "Azure/aks/azurerm"
  version = "6.4.0"

  prefix              = local.prefix
  resource_group_name = var.resource_group_name
}

output "host" {
  value     = module.aks.host
  sensitive = true
}

output "client_certificate" {
  value     = base64decode(module.aks.client_certificate)
  sensitive = true
}

output "client_key" {
  value     = base64decode(module.aks.client_key)
  sensitive = true
}

output "cluster_ca_certificate" {
  value     = base64decode(module.aks.cluster_ca_certificate)
  sensitive = true
}

resource "azurerm_role_assignment" "ingress_role" {
  scope                = var.resource_grouo_id
  role_definition_name = "Network Contributor"
  principal_id         = module.aks.cluster_identity.principal_id
}

resource "azurerm_role_assignment" "acr" {

  scope                = var.azure_acr_id
  role_definition_name = "AcrPull"
  principal_id         = module.aks.kubelet_identity[0].object_id
}
