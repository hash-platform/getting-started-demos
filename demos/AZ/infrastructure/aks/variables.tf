variable "prefix" {
  description = "The used prefix for this env"
}

variable "environment" {
  description = "The name of this env"
}

variable "subscription_id" {
  description = "The ID of used subscription"
}
