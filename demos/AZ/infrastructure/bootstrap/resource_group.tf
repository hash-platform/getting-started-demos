variable "location" {
  description = "The location of resource group"
  default     = "West Europe"
}

locals {
  resource_group_name = "${var.prefix}-${var.environment}"
}

resource "azurerm_resource_group" "main" {
  name     = local.resource_group_name
  location = var.location
}

output "resource_group_name" {
  value = local.resource_group_name
}

output "resource_group_id" {
  value = azurerm_resource_group.main.id
}
