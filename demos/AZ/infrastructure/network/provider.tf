terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.37.0"
    }
  }
}

provider "azurerm" {
  features {}

  subscription_id = var.subscription_id
}

provider "azurerm" {
  alias = "dns"
  features {}

  subscription_id = "cef1bbbb-20aa-4215-a350-7e7fc1cb5112"
}
