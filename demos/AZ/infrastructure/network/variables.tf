variable "environment" {
  description = "The name of environment"
}

variable "subscription_id" {
  description = "The name of subscription"
}

variable "location" {
  description = "The location of resources"
  default     = "West Europe"
}
