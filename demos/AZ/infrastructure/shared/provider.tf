terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.37.0"
    }
  }
}

provider "azurerm" {
  features {}

  subscription_id = var.subscription_id
}

variable "subscription_id" {
  description = "The ID of subscription used in this resource"
}

variable "location" {
  description = "The location of resources"
  default     = "West Europe"
}
