resource "azurerm_resource_group" "registry" {
  name     = "docker-registry"
  location = var.location
}

resource "azurerm_container_registry" "main" {
  name                = "hashcoredemo"
  resource_group_name = azurerm_resource_group.registry.name
  location            = var.location
  sku                 = "Premium"
}

output "acr_id" {
  value = azurerm_container_registry.main.id
}

output "registry_url" {
  value = azurerm_container_registry.main.login_server
}
