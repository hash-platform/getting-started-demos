# Variables

variable "k8s_name" {
  description = "The name of k8s cluster"
}

variable "k8s_region" {
  description = "The region where to create the cluster"
  default     = "fra1"
}

variable "k8s_version" {
  description = "The version of kubernetes cluster, when empty use latest version"
  default     = ""
}

variable "k8s_node_pool_size" {
  description = "The size of the default node pool"
}

variable "k8s_node_pool_node_count" {
  description = "The nodes' count on the node pool"
}

variable "k8s_node_pool_name" {
  description = "The name of the default node pool"
  default     = "worker-pool"
}

data "digitalocean_kubernetes_versions" "versions" {}

resource "digitalocean_kubernetes_cluster" "main" {
  name   = var.k8s_name
  region = var.k8s_region
  # Grab the latest version slug from `doctl kubernetes options versions`
  version              = var.k8s_version != "" ? var.k8s_version : data.digitalocean_kubernetes_versions.versions.latest_version
  registry_integration = true

  node_pool {
    name       = var.k8s_node_pool_name
    size       = var.k8s_node_pool_size
    node_count = var.k8s_node_pool_node_count
  }
}

# Outputs

output "k8s_host" {
  description = "The API server URL for main kubernetes cluster"
  value       = digitalocean_kubernetes_cluster.main.kube_config.0.host
  sensitive   = true
}

output "k8s_certificate_ca" {
  description = "The certificate of the CA used by main kubernetes cluster to sign its certificate"
  value       = base64decode(digitalocean_kubernetes_cluster.main.kube_config.0.cluster_ca_certificate)
  sensitive   = true
}

output "k8s_token" {
  description = "The token used by main kubernetes cluster"
  value       = digitalocean_kubernetes_cluster.main.kube_config.0.token
  sensitive   = true
}

output "raw" {
  description = "Raw config of main kubernetes cluster"
  value       = digitalocean_kubernetes_cluster.main.kube_config.0.raw_config
  sensitive   = true
}
