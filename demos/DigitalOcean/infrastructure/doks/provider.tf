terraform {
  required_version = ">= 1.0.0, < 2.0.0"
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = ">=2.28.1,<3.0.0"
    }
  }
}

provider "digitalocean" {}
