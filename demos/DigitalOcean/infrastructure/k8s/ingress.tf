resource "kubernetes_ingress_v1" "main_ingress" {
  metadata {
    name      = "main-ingress"
    namespace = kubernetes_namespace.istio_system.metadata.0.name
    labels = {
      "managed-by"    = "terraform"
      "hash/resource" = "do-k8s"
    }
    annotations = {
      "cert-manager.io/cluster-issuer" = "letsencrypt"
    }
  }

  spec {
    ingress_class_name = "nginx"
    default_backend {
      service {
        name = "istio-ingress"
        port {
          number = 80
        }
      }
    }
    tls {
      secret_name = "app-tls"
      hosts       = ["app.${var.environment}.do.mouhsen.de"]
    }
  }
}

variable "environment" {
  description = "The name of environment to deploy k8s to it"
}
