resource "kubernetes_namespace" "ngin_ingress" {
  metadata {
    name = "nginx-ingress"
    labels = {
      "managed-by"    = "terraform"
      "hash/resource" = "do-k8s"
    }
  }
}

resource "helm_release" "ngin_ingress" {
  name       = "nginx-ingress"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = "4.4.2"
  namespace  = kubernetes_namespace.ngin_ingress.metadata.0.name

  values = [templatefile("nginx-ingress.tftpl", {})]
}
