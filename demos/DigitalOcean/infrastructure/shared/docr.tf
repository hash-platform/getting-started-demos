# Variables

variable "docr_name" {
  description = "The name of digital ocean container registry"
  default     = "shared"
}

resource "digitalocean_container_registry" "main" {
  name                   = var.docr_name
  subscription_tier_slug = "starter"
}

resource "digitalocean_container_registry_docker_credentials" "main" {
  registry_name = digitalocean_container_registry.main.name
  write         = true
}

output "server_url" {
  description = "The server URl for main container regsitry"
  value       = digitalocean_container_registry.main.endpoint
}

output "docker_config" {
  description = "The docker config file for the main container regsitry"
  value       = digitalocean_container_registry_docker_credentials.main.docker_credentials
  sensitive   = true
}
