package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Welcome to my website, hash platform - main 2")
	})

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("cannot listen to port 8080")
	}
}
