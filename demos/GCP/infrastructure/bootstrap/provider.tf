terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.45.0"
    }
  }
}

provider "google" {
  project = var.gcp_project
}

variable "gcp_project" {
  description = "The name of GCP project to deploy to it"
}
