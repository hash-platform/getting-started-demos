resource "google_project_service" "enabled_services" {
  for_each = toset([
    "compute.googleapis.com",
    "container.googleapis.com",
    "dns.googleapis.com",
  ])

  service = each.key

  project                    = var.gcp_project
  disable_on_destroy         = true
  disable_dependent_services = true
}
