locals {
  gke_name = var.prefix != "" ? "${var.prefix}-${var.gke_name}" : var.gke_name
}

variable "zones" {
  description = "The zones to deploy GKE to them"
  default     = ["europe-west3-a", "europe-west3-b", "europe-west3-c"]
}

module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  project_id                 = var.gcp_project
  name                       = local.gke_name
  region                     = var.region
  zones                      = var.zones
  network                    = var.network_id
  subnetwork                 = var.subnet_id
  ip_range_pods              = "pod-ips"
  ip_range_services          = "svc-ips"
  http_load_balancing        = true
  network_policy             = false
  horizontal_pod_autoscaling = true
  filestore_csi_driver       = false
  enable_private_endpoint    = false
  enable_private_nodes       = true
  master_ipv4_cidr_block     = "10.0.0.0/28"
  release_channel            = "STABLE"
  remove_default_node_pool   = true

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "e2-medium"
      node_locations     = var.zones[0]
      min_count          = 1
      max_count          = 3
      local_ssd_count    = 0
      spot               = false
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS_CONTAINERD"
      enable_gcfs        = false
      enable_gvnic       = false
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = true
      initial_node_count = 1
    },
  ]

  node_pools_oauth_scopes = {
    all = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_tags = {
    all = []

    default-node-pool = [
      "default-node-pool",
    ]
  }
}

output "endpoint" {
  value     = module.gke.endpoint
  sensitive = true
}

output "ca_certificate" {
  value     = base64decode(module.gke.ca_certificate)
  sensitive = true
}

resource "google_artifact_registry_repository_iam_member" "gke_repo_reader" {
  provider   = google-beta
  project    = var.artifact_registry_project
  location   = var.artifact_regitry_location
  repository = var.artifact_registry_repository
  role       = "roles/artifactregistry.reader"
  member     = "serviceAccount:${module.gke.service_account}"
}
