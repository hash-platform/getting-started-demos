resource "google_service_account" "cert_manager" {
  account_id = "cert-manager"
}

resource "google_service_account_iam_binding" "cert_manager" {
  service_account_id = google_service_account.cert_manager.name
  role               = "roles/iam.workloadIdentityUser"

  members = [
    "serviceAccount:${var.gcp_project}.svc.id.goog[cert-manager/cert-manager]"
  ]
}

resource "google_project_iam_member" "cert_manager" {
  project = var.gcp_project
  role    = "roles/dns.admin"
  member  = "serviceAccount:${google_service_account.cert_manager.email}"
}

output "cert_manager_sa_email" {
  value = google_service_account.cert_manager.email
}
