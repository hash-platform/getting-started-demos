variable "prefix" {
  description = "A unique prefix for upper level resources created in en env"
}

variable "gcp_project" {
  description = "The name of GCP project to deploy to it"
}

variable "gke_name" {
  description = "The name of GKE cluster to use"
}

output "project_id" {
  value = var.gcp_project
}
