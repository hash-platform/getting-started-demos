resource "google_dns_managed_zone" "main" {
  name        = "main-${var.environment}"
  dns_name    = "${var.environment}.gcp.mouhsen.de."
  description = "Main DNS zone for ${var.environment}"
  labels = {
    "managed-by"    = "terraform"
    "hash-resource" = "network"
    "environment"   = var.environment
  }
}

resource "google_dns_record_set" "env_delegation" {
  name = google_dns_managed_zone.main.dns_name
  type = "NS"
  ttl  = 300

  managed_zone = "mouhsen-de"
  project      = "mouhsen-dns-main"

  rrdatas = google_dns_managed_zone.main.name_servers
}


resource "google_dns_record_set" "app" {
  name = "app.${google_dns_managed_zone.main.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.main.name

  rrdatas = [google_compute_global_address.ingress_ip.address]
}

output "name_servers" {
  value = google_dns_managed_zone.main.name_servers
}
