resource "google_compute_firewall" "firewall" {
  name    = "${var.prefix}-firewall"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "tcp"
    ports    = ["15017"]
  }

  source_ranges = [
    "10.0.0.0/28"
  ]
}

resource "google_compute_firewall" "firewall_hc" {
  name    = "${var.prefix}-firewall-hc"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "tcp"
    ports    = ["15021"]
  }

  source_ranges = [
    "35.191.0.0/16",
    "130.211.0.0/22"
  ]
}
