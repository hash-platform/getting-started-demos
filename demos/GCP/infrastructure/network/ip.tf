resource "google_compute_global_address" "ingress_ip" {
  name = "ingress-ip-${var.environment}"
}

output "ingress_ip_name" {
  value = google_compute_global_address.ingress_ip.name
}

output "ingress_ip_address" {
  value = google_compute_global_address.ingress_ip.address
}
