terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.45.0"
    }
  }
}

provider "google" {
  project = var.gcp_project
  region  = var.region
}

variable "region" {
  description = "The default GCP region"
  default     = "europe-west3"
}
