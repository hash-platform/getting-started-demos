variable "prefix" {
  description = "A unique prefix for upper level resources created in en env"
}

variable "environment" {
  description = "The name of env where this resource is deployed"
}

variable "gcp_project" {
  description = "The name of GCP project to deploy to it"
}
