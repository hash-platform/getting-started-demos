locals {
  vpc_name = var.prefix != "" ? "${var.prefix}-vpc" : "vpc"
}
resource "google_compute_network" "vpc_network" {
  project                 = var.gcp_project
  name                    = local.vpc_name
  auto_create_subnetworks = false
  mtu                     = 1460
}

resource "google_compute_subnetwork" "vpc_subnetwork" {
  name          = "gke"
  ip_cidr_range = "10.181.0.0/22"
  region        = var.region
  network       = google_compute_network.vpc_network.id
  secondary_ip_range {
    range_name    = "pod-ips"
    ip_cidr_range = "10.48.0.0/14"
  }
  secondary_ip_range {
    range_name    = "svc-ips"
    ip_cidr_range = "10.2.0.0/20"
  }
}

output "vpc_name" {
  value = google_compute_network.vpc_network.name
}

output "vpc_subnet_name" {
  value = google_compute_subnetwork.vpc_subnetwork.name
}
