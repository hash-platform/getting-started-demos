terraform {
  required_providers {
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 4.16.0"
    }
  }
}

provider "google-beta" {
  project = var.project
  region  = var.region
}

variable "project" {
  description = "The GCP project used in this terraform config"
}

variable "region" {
  description = "The GCP region used in this terraform config"
  default     = "europe-west3"
}
