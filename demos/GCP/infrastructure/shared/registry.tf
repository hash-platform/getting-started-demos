resource "google_project_service" "apis" {
  project = var.project
  service = "artifactregistry.googleapis.com"
}

resource "google_service_account" "repo_reader_account" {
  account_id   = "repo-reader"
  display_name = "A service account with read permissions for shared docker registry"
  project      = var.project
}

resource "google_service_account" "repo_writer_account" {
  account_id   = "repo-writer"
  display_name = "A service account with read permissions for shared docker registry"
  project      = var.project
}

data "google_client_openid_userinfo" "me" {
}

resource "google_service_account_iam_member" "writer-account-user" {
  service_account_id = google_service_account.repo_writer_account.name
  role               = "roles/iam.serviceAccountTokenCreator"
  member             = "user:${data.google_client_openid_userinfo.me.email}"
}

resource "google_artifact_registry_repository" "repo" {
  provider = google-beta

  location      = var.region
  repository_id = "shared"
  description   = "Shared docker repository"
  format        = "DOCKER"
  depends_on = [
    google_project_service.apis
  ]
}

resource "google_artifact_registry_repository_iam_member" "repo_reader" {
  provider   = google-beta
  project    = google_artifact_registry_repository.repo.project
  location   = google_artifact_registry_repository.repo.location
  repository = google_artifact_registry_repository.repo.name
  role       = "roles/artifactregistry.reader"
  member     = "serviceAccount:${google_service_account.repo_reader_account.email}"
}

resource "google_artifact_registry_repository_iam_member" "repo_writer" {
  provider   = google-beta
  project    = google_artifact_registry_repository.repo.project
  location   = google_artifact_registry_repository.repo.location
  repository = google_artifact_registry_repository.repo.name
  role       = "roles/artifactregistry.writer"
  member     = "serviceAccount:${google_service_account.repo_writer_account.email}"
}


output "registry_host" {
  description = "The hostname for docker regsitry, it is used in cred helpers"
  value       = "${var.region}-docker.pkg.dev"
}

output "registry_url" {
  description = "The URL to docker registry, used in docker build and push commands"
  value       = "${var.region}-docker.pkg.dev/${var.project}/shared"
}

output "repo_writer_email" {
  description = "The email of service account with write permission to GCR"
  value       = google_service_account.repo_writer_account.email
}

output "repo_location" {
  value = google_artifact_registry_repository.repo.location
}

output "repo_name" {
  value = google_artifact_registry_repository.repo.name
}

output "repo_project" {
  value = var.project
}
