package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Welcome to my website, hash platform - GKE")
	})

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatalln("error in listneing to port 8080")
	}
}
